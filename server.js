const http = require('http');
const path = require('path');
const fs = require('fs');


const server = http.createServer((req, res) => {
    const getApiUsers = () => {
        const users = [
            { name: 'A', age: 40 },
            { name: 'B', age: 34 }
        ]
        res.writeHead(200, { 'Content-Type': 'applicaation/json' })
        res.end(JSON.stringify(users));
        console.log("api/user")
    }
    switch (req.url) {
        case '/api/users':
            getApiUsers();
            break;
        default: {
            let filePath = path.join(__dirname, 'public', (req.url === '/' ? 'index.html' : req.url));
            contentType = 'text/html';
            switch (path.extname(filePath)) {
                case '.js':
                    contentType = 'text/javascript';
                    break;
                case '.css':
                    contentType = 'text/css';
                    break;
                case '.json':
                    contentType = 'application/json';
                    break;
                case '.png':
                    contentType = 'image/png';
                    break;
                case '.jpg':
                    contentType = 'image/jpg';
                    break;
            }
            fs.readFile(
                filePath,
                (err, content) => {
                    if (err) {
                        if (err.code == 'ENOENT') {
                            fs.readFile(path.join(__dirname, 'public', '404.html'), (err, content) => {
                                if (err) throw err
                                res.writeHead(200, { 'Content-Type': 'text/html' })
                                res.end(content, 'utf8');
                                console.log("Не найден файл :(")
                            })
                        } else {
                            res.writeHead(500)
                            res.end('Server Error:' + err.code);
                            console.log("Ошибка сервера")
                        }
                    }
                    else {
                        res.writeHead(200, { 'Content-Type': contentType })
                        res.end(content);
                        console.log("Вернули файл " + filePath)
                    }
                }
            )
            break;
        }
    }

})

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log("Server started on http://localhost:" + PORT);
})